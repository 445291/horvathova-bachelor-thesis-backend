# BackEnd

This project is a part of an implementation for my bachelors theses. It is a REST API server for **Network Topology Editor**.

## Preparation

First install *Python 3* on your computer. Download this project and inside your folder for this project run `pip install -r requirements.txt`. Then install *PostgreSQL* and in project's file `settings.py` change strings you chose for connection.

## Running REST API server

Make sure Postgres is running and then to start backend server run command `python manage.py runserver`.

## Database

For list of all saved graphs visit `http://localhost:8000/rest/all`.

For all details of a saved graph visit `http://localhost:8000/rest/detail?filename=NAME_OF_A_SAVED_GRAPH`.

## Making migrations

To make migrations run `python manage.py makemigrations rest` and to migrate database run `python manage.py migrate rest`.