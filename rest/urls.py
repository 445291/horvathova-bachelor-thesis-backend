from django.conf.urls import url

from . import views

urlpatterns = [
    url('all', views.graphs_list, name='all'),
    url('detail', views.graph_detail, name='detail'),
]
