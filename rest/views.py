from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

from rest.serializers import *
from rest.validation.validation_service import ValidationService


@csrf_exempt
def graphs_list(request):
    """Resolves requests for URL 'rest/all'."""

    if request.method == 'GET':
        # Lists all graphs.
        graphs = Graph.objects.all()
        serializer = GraphLimitedSerializer(graphs, many=True)
        return JsonResponse(serializer.data, safe=False)

    if request.method == 'POST':
        # Creates new or updates graph.
        data = JSONParser().parse(request)

        serializer = GraphDetailSerializer(data=data)
        if serializer.is_valid():
            validation_service = ValidationService()
            validation_result = validation_service.is_valid(serializer.validated_data)

            if validation_result[0]:
                # Deletes graph with same name if exists.
                graph_name = data['name']

                if Graph.objects.filter(name=graph_name).exists():
                    graph = Graph.objects.get(name=graph_name)
                    for role in graph.roles.all():
                        role.delete()
                    graph.delete()

                serializer.save()
                return JsonResponse(serializer.data, status=201)
            else:
                print('Validation has failed for graph.', serializer.validated_data)
                return JsonResponse(validation_result[1], status=400)
        return JsonResponse(serializer.errors, status=400)

    if request.method == 'DELETE':
        # Deletes graph.
        graph = Graph.objects.get(name=request.GET.get('filename', None))
        for role in graph.roles.all():
            role.delete()
        graph.delete()
        return HttpResponse(status=204)


@csrf_exempt
def graph_detail(request):
    """Resolves requests for URL 'rest/detail'."""

    if request.method == 'GET':
        # Gets graph details.
        graph = Graph.objects.get(name=request.GET.get('filename', None))
        serializer = GraphDetailSerializer(graph)
        return JsonResponse(serializer.data, safe=False)

    if request.method == 'PATCH':
        # Renames graph.
        data = JSONParser().parse(request)

        filename = data['filename']
        new_filename = data['new_filename']

        # Checks if graph with same name exists.
        if Graph.objects.filter(name=new_filename).exists():
            return JsonResponse({
                'message': 'Graph with that filename already exists.'
            }, safe=False, status=400)

        graph = Graph.objects.get(name=filename)
        graph.name = new_filename
        graph.save()
        serializer = GraphDetailSerializer(graph)
        return JsonResponse(serializer.data, safe=False)
