from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from .models import Graph, Router, Host, Link


class HostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Host
        fields = ('class_name', 'object_id', 'x', 'y', 'data_name', 'data_ip',
                  'data_role', 'data_vcn_password', 'data_image_id', 'data_group', 'data_note')


class RouterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Router
        fields = ('class_name', 'object_id', 'x', 'y', 'data_name', 'data_ip',
                  'data_prefix', 'data_image_id', 'data_note')


class RolePolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Host: HostSerializer,
        Router: RouterSerializer,
    }


class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ('object_id', 'source_id', 'target_id', 'data_note')


class GraphLimitedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Graph
        fields = ('id', 'name')


class GraphDetailSerializer(serializers.ModelSerializer):
    roles = RolePolymorphicSerializer(many=True)
    links = LinkSerializer(many=True)

    class Meta:
        model = Graph
        fields = ('id', 'name', 'roles', 'links')

    def create(self, validated_data):
        roles_data = validated_data.pop('roles')
        links_data = validated_data.pop('links')
        graph = Graph.objects.create(**validated_data)

        serializer = RolePolymorphicSerializer()
        for role_data in roles_data:
            role_data['graph_id'] = graph.id
            serializer.create(role_data)

        for link_data in links_data:
            Link.objects.create(graph=graph, **link_data)

        return graph
