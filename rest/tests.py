import json
import os

from rest_framework import status
from rest_framework.test import APITestCase

from .models import Graph, Router, Host, Link

EXPECTED_DATA = os.path.join(os.path.dirname(__file__), 'test_data/expected_data.json')
INPUT_DATA = os.path.join(os.path.dirname(__file__), 'test_data/input_data.json')


class SimpleTest(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.expected_data = json.load(open(EXPECTED_DATA))
        cls.input_data = json.load(open(INPUT_DATA))

        cls.graph_1 = Graph.objects.create(
            name='test_graph_1'
        )
        cls.router = Router.objects.create(
            graph=cls.graph_1,
            class_name='RouterComponent',
            object_id=0,
            x=1388,
            y=828,
            data_name='router1',
            data_ip='1.1.1.0',
            data_prefix='24',
            data_image_id='',
            data_note=''
        )
        cls.host = Host.objects.create(
            graph=cls.graph_1,
            class_name='DesktopComponent',
            object_id=1,
            x=1378,
            y=1148,
            data_name='desktop1',
            data_ip='1.1.1.1',
            data_role='attacker',
            data_vcn_password='secretPassword1010',
            data_image_id='',
            data_group='',
            data_note='My computer.'
        )
        cls.link = Link.objects.create(
            graph=cls.graph_1,
            object_id=2,
            source_id=0,
            target_id=1,
            data_note=''
        )

    def test_graph_list_get(self):
        response = self.client.get('/rest/all', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content.decode('utf-8'), '[{"id": 1, "name": "test_graph_1"}]')

    def test_graph_list_post_valid(self):
        response = self.client.post('/rest/all', self.input_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Graph.objects.count(), 2)
        self.assertEqual(Graph.objects.get(name='test_graph_2').name, 'test_graph_2')

    def test_graph_list_post_invalid(self):
        response = self.client.post('/rest/all', self.expected_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Graph.objects.count(), 1)

    def test_graph_list_delete(self):
        response = self.client.delete('/rest/all?filename=test_graph_1')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Graph.objects.count(), 0)

    def test_graph_detail_get(self):
        response = self.client.get('/rest/detail?filename=test_graph_1', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content.decode('utf-8')), self.expected_data)

    def test_graph_detail_patch_valid(self):
        response = self.client.patch('/rest/detail', data={
            'filename': 'test_graph_1',
            'new_filename': 'new_test_graph_1'
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_graph_detail_patch_invalid(self):
        response = self.client.patch('/rest/detail', data={
            'filename': 'test_graph_1',
            'new_filename': 'test_graph_1'
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
