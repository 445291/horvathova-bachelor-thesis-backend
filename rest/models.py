from django.db import models
from polymorphic.models import PolymorphicModel


class Graph(models.Model):
    name = models.CharField(max_length=50)


class Role(PolymorphicModel):
    graph = models.ForeignKey(Graph, related_name='roles', on_delete=models.CASCADE)
    class_name = models.CharField(max_length=50)
    object_id = models.IntegerField()
    x = models.FloatField()
    y = models.FloatField()
    data_name = models.CharField(max_length=50, blank=True)
    data_ip = models.CharField(max_length=50, blank=True)


class Host(Role):
    data_role = models.CharField(max_length=50, blank=True)
    data_vcn_password = models.CharField(max_length=50, blank=True)
    data_image_id = models.CharField(max_length=50, blank=True)
    data_group = models.CharField(max_length=50, blank=True)
    data_note = models.CharField(max_length=50, blank=True)


class Router(Role):
    data_prefix = models.CharField(max_length=50, blank=True)
    data_image_id = models.CharField(max_length=50, blank=True)
    data_note = models.CharField(max_length=50, blank=True)


class Link(models.Model):
    graph = models.ForeignKey(Graph, related_name='links', on_delete=models.CASCADE)
    object_id = models.IntegerField()
    source_id = models.IntegerField()
    target_id = models.IntegerField()
    data_note = models.CharField(max_length=50, blank=True)
