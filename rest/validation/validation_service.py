from rest.validation.validation_rule import TwoRouterValidationRule


class ValidationService(object):
    """Service for validating graphs with our own rules."""

    def __init__(self):
        self.validation_rules = []
        self._add_rules()

    def _add_rules(self):
        """Adds rules defined in 'validation_rule.py' we want to check."""
        self.validation_rules.append(TwoRouterValidationRule())

    def is_valid(self, graph):
        """Checks if graph is valid with all defined rules."""
        errors = {
            'name': []
        }

        for rule in self.validation_rules:
            if not rule.is_valid(graph):
                errors['name'].append(rule.get_message())

        if errors['name']:
            return False, errors
        else:
            return True, None
