import abc


class ValidationRule(metaclass=abc.ABCMeta):
    """Abstract class for rules."""

    @abc.abstractmethod
    def is_valid(self, graph):
        pass

    @abc.abstractmethod
    def get_message(self):
        pass


class TwoRouterValidationRule(ValidationRule):
    """Rule for at least two routers in graph."""

    def is_valid(self, graph):
        """Checks if graph valid with this rule."""
        count = 0
        for role in graph['roles']:
            if role['class_name'] == 'RouterComponent':
                count += 1
        return True if count >= 2 else False

    def get_message(self):
        """Returns informative message if rule is broken."""
        return 'Minimum number of routers in graph is 2.'
